import { IFilm } from "../entities/films";
import { IPerson, TGender } from "../entities/people";
import { IPlanet } from "../entities/planet";
import { TResourceName } from "../entities/resource";
import { IStarship } from "../entities/starships";
import { IKeyValuePair } from "../utils-types/KeyValuePair";

export class TranslateResourceProperties {
  public translateResource(
    type: TResourceName,
    resource: IFilm | IPerson | IPlanet | IStarship,
  ): IKeyValuePair[] {
    switch (type) {
      case "films":
        return this.translateFilm(resource as IFilm);
      case "people":
        return this.translatePerson(resource as IPerson);
      case "planets":
        return this.translatePlanet(resource as IPlanet);
      case "starships":
        return this.translateStarship(resource as IStarship);

      default:
        throw new Error("Error while translating resource");
    }
  }

  private translateFilm(film: IFilm): IKeyValuePair[] {
    return [
      { key: "Título", value: film.title },
      {
        key: "Fecha de lanzamiento",
        value: new Date(film.release_date).toLocaleDateString(),
      },
      { key: "Director", value: film.director },
    ];
  }

  private translatePerson(person: IPerson): IKeyValuePair[] {
    return [
      { key: "Nombre", value: person.name },
      { key: "Genero", value: this.genderTranslation(person.gender) },
      { key: "Fecha de nacimiento", value: person.birth_year },
      { key: "Color de ojos", value: person.eye_color },
      { key: "Color de pelo", value: person.hair_color },
      { key: "Altura", value: person.height },
      { key: "Peso", value: person.mass },
    ];
  }

  private genderTranslation(gender: TGender) {
    let genderTranslation = "Sin genero";
    if (gender == "male") {
      genderTranslation = "Masculino";
    } else if (gender == "female") {
      genderTranslation = "Femenino";
    }

    return genderTranslation;
  }

  private translatePlanet(planet: IPlanet): IKeyValuePair[] {
    return [
      { key: "Nombre", value: planet.name },
      { key: "Clima", value: planet.climate },
      { key: "Diametro", value: planet.diameter },
      { key: "Fuerza de Gravedad", value: planet.gravity },
      { key: "poblacion", value: planet.population },
      { key: "Periodo de rotacion", value: planet.rotation_period },
      { key: "Terreno", value: planet.terrain },
      { key: "% de agua", value: planet.surface_water },
    ];
  }

  private translateStarship(starship: IStarship): IKeyValuePair[] {
    return [
      { key: "Nombre", value: starship.name },
      { key: "Modelo", value: starship.model },
      { key: "Clase", value: starship.starship_class },
      { key: "Megalights/h", value: starship.MGLT },
      { key: "Capacidad de carga", value: starship.cargo_capacity },
      { key: "Costo", value: starship.cost_in_credits },
      { key: "Cantidad de tripulacion", value: starship.crew },
      { key: "Clase de hyperdrive", value: starship.hyperdrive_rating },
      { key: "Fabricantes", value: starship.manufacturer },
      { key: "Vel. Max. Atmosfera", value: starship.max_atmosphering_speed },
    ];
  }
}
