import { FavoriteResourceRepositoryMock } from "../mocks/FavoriteResourceRepository.mock";
import { DeleteFavoriteResource } from "./DeleteFavoriteResource";

describe("DeleteFavoriteResource Interactor Tests", () => {
  const mockRepository = new FavoriteResourceRepositoryMock();
  mockRepository.save(
    { id: "mockId", name: "mockName", created: "", edited: "" },
    "people",
  );

  it("Deletes favorite resource", () => {
    const initialState = mockRepository.getAll();
    const deleteFavorite = new DeleteFavoriteResource(mockRepository);
    expect(initialState.length).toBe(1);
    deleteFavorite.delete("mockId", "people");
    const afterDelete = mockRepository.getAll();
    expect(afterDelete.length).toBe(0);
  });
});
