import { IResourceRepository } from "../adapters/repositories/IResourceRepository";
import {
  TPaginatedResourceResponse,
  TResourceName,
} from "../entities/resource";

export class GetAllResource {
  private resourceRepository: IResourceRepository;

  constructor(ResourceRepository: IResourceRepository) {
    this.resourceRepository = ResourceRepository;
  }

  async get<T extends TResourceName>(
    resource: T,
    page: number,
    query: string | undefined,
  ): Promise<TPaginatedResourceResponse> {
    return await this.resourceRepository.getAll(resource, page, query);
  }
}
