import {
  IResourceRepository,
  TResourceGetOneResult,
} from "../adapters/repositories/IResourceRepository";
import { TResourceName } from "../entities/resource";

export class GetOneResource {
  private resourceRepository: IResourceRepository;

  constructor(ResourceRepository: IResourceRepository) {
    this.resourceRepository = ResourceRepository;
  }

  async get<T extends TResourceName>(
    resource: T,
    id: string,
  ): Promise<TResourceGetOneResult<T>> {
    return await this.resourceRepository.getOne(resource, id);
  }
}
