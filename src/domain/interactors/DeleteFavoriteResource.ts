import { IFavoriteRepository } from "../adapters/repositories/IFavoritesRepository";
import { SavedResource, TResourceName } from "../entities/resource";

export class DeleteFavoriteResource {
  private favoriteRespository: IFavoriteRepository;

  constructor(FavoriteRepository: IFavoriteRepository) {
    this.favoriteRespository = FavoriteRepository;
  }

  delete(resourceId: string, type: TResourceName): SavedResource[] {
    return this.favoriteRespository.delete(resourceId, type);
  }
}
