import { IFavoriteRepository } from "../adapters/repositories/IFavoritesRepository";
import { Resource, SavedResource, TResourceName } from "../entities/resource";

export class SaveFavoriteResource {
  private favoriteRespository: IFavoriteRepository;

  constructor(FavoriteRepository: IFavoriteRepository) {
    this.favoriteRespository = FavoriteRepository;
  }

  save(resource: Resource, type: TResourceName): SavedResource[] {
    return this.favoriteRespository.save(resource, type);
  }
}
