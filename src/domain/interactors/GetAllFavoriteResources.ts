import { IFavoriteRepository } from "../adapters/repositories/IFavoritesRepository";
import { SavedResource } from "../entities/resource";

export class GetAllFavoriteResources {
  private favoriteRespository: IFavoriteRepository;

  constructor(FavoriteRepository: IFavoriteRepository) {
    this.favoriteRespository = FavoriteRepository;
  }

  get(): SavedResource[] {
    return this.favoriteRespository.getAll();
  }
}
