import { IFavoriteRepository } from "../adapters/repositories/IFavoritesRepository";
import { TResourceName, SavedResource, Resource } from "../entities/resource";

export class FavoriteResourceRepositoryMock implements IFavoriteRepository {
  private favoriteStore: SavedResource[] = [];
  delete(id: string, type: TResourceName): SavedResource[] {
    const resourceIndex = this.favoriteStore.findIndex(
      (resource) => resource.id == id && resource.type == type,
    );

    if (resourceIndex != -1) {
      this.favoriteStore.splice(resourceIndex, 1);
    }

    return this.favoriteStore;
  }

  getAll(): SavedResource[] {
    return this.favoriteStore;
  }

  save(resource: Resource, type: TResourceName): SavedResource[] {
    this.favoriteStore.push({ ...resource, type });
    return this.favoriteStore;
  }
}
