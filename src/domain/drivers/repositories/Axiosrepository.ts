import axios, { AxiosInstance } from "axios";

export class AxiosRepository {
  client(): AxiosInstance {
    const axiosInstance = axios.create({
      baseURL: this.getBaseURL(),
    });

    return axiosInstance;
  }

  protected getBaseURL(): string {
    return typeof window === "undefined"
      ? `${process.env.BACKEND_URL}/api/v1`
      : "/api";
  }
}
