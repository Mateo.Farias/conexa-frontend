import {
  TPaginatedResourceResponse,
  TResourceName,
} from "@/domain/entities/resource";
import { AxiosRequestConfig } from "axios";
import {
  IResourceRepository,
  TResourceGetOneResult,
} from "../../adapters/repositories/IResourceRepository";
import { AxiosRepository } from "./Axiosrepository";

export class ResourceRepository
  extends AxiosRepository
  implements IResourceRepository
{
  async getAll<T extends TResourceName>(
    resource: T,
    page: number,
    query?: string,
  ): Promise<TPaginatedResourceResponse> {
    const config: AxiosRequestConfig = {
      params: {
        page,
        query,
      },
    };

    return this.client()
      .get(`/${resource}`, config)
      .then((response) => response.data)
      .catch((reason) => {
        console.log(`${ResourceRepository}-${resource}-GetAll error: `, reason);
      });
  }

  async getOne<T extends TResourceName>(
    resource: T,
    id: string,
  ): Promise<TResourceGetOneResult<T>> {
    return this.client()
      .get(`/${resource}/${id}`)
      .then((response) => response.data)
      .catch((reason) => {
        console.log(`${ResourceRepository}-${resource}-GetOne error: `, reason);
      });
  }
}
