import { IFavoriteRepository } from "@/domain/adapters/repositories/IFavoritesRepository";
import {
  Resource,
  SavedResource,
  TResourceName,
} from "@/domain/entities/resource";

export class FavoriteLocalStorageRepository implements IFavoriteRepository {
  private storageKey = "favorites";
  save(resource: Resource, type: TResourceName): SavedResource[] {
    const currentState = this.getSavedFavorites();
    const savedResourceIndex = this.findSavedResourceIndex(
      currentState,
      resource.id,
      type,
    );
    if (savedResourceIndex == -1) {
      currentState.push({ ...resource, type });
    }

    return this.saveResourcesToLocalStorage(currentState);
  }

  delete(id: string, type: TResourceName): SavedResource[] {
    const currentState = this.getSavedFavorites();
    const savedResourceIndex = this.findSavedResourceIndex(
      currentState,
      id,
      type,
    );
    if (savedResourceIndex != -1) {
      currentState.splice(savedResourceIndex, 1);
    }

    return this.saveResourcesToLocalStorage(currentState);
  }

  getAll(): SavedResource[] {
    return this.getSavedFavorites();
  }

  private saveResourcesToLocalStorage(resources: SavedResource[]) {
    window.localStorage.setItem(this.storageKey, JSON.stringify(resources));
    return resources;
  }

  private getSavedFavorites(): SavedResource[] {
    const favoriteStore = window.localStorage.getItem(this.storageKey);
    if (favoriteStore) {
      return JSON.parse(favoriteStore);
    } else {
      return [];
    }
  }

  private findSavedResourceIndex(
    currentState: SavedResource[],
    resourceId: string,
    type: TResourceName,
  ): number {
    return currentState.findIndex(
      (savedResource) =>
        savedResource.id == resourceId && savedResource.type == type,
    );
  }
}
