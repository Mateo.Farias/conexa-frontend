import { GetAllResource } from "@/domain/interactors/GetAllResource";
import { GetOneResource } from "@/domain/interactors/GetOneResource";
import { ResourceRepository } from "../repositories/ResourceRepository";

export class ResourceConfiguration {
  private resourceRepository = new ResourceRepository();

  getAllResource(): GetAllResource {
    return new GetAllResource(this.resourceRepository);
  }

  getOneResource(): GetOneResource {
    return new GetOneResource(this.resourceRepository);
  }
}
