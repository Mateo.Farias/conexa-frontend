import { DeleteFavoriteResource } from "@/domain/interactors/DeleteFavoriteResource";
import { GetAllFavoriteResources } from "@/domain/interactors/GetAllFavoriteResources";
import { SaveFavoriteResource } from "@/domain/interactors/SaveFavoriteResource";
import { FavoriteLocalStorageRepository } from "../repositories/FavoriteLocalStorageRepository";

export class FavoriteResourceConfiguration {
  private favoriteRepository = new FavoriteLocalStorageRepository();

  saveFavorite(): SaveFavoriteResource {
    return new SaveFavoriteResource(this.favoriteRepository);
  }

  deleteFavorite(): DeleteFavoriteResource {
    return new DeleteFavoriteResource(this.favoriteRepository);
  }

  getAllFavorites(): GetAllFavoriteResources {
    return new GetAllFavoriteResources(this.favoriteRepository);
  }
}
