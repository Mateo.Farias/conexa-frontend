import {
  Resource,
  SavedResource,
  TResourceName,
} from "@/domain/entities/resource";

export interface IFavoriteRepository {
  save(resource: Resource, type: TResourceName): SavedResource[];
  delete(id: string, type: TResourceName): SavedResource[];
  getAll(): SavedResource[];
}
