import { IFilm } from "@/domain/entities/films";
import { IPerson } from "@/domain/entities/people";
import { IPlanet } from "@/domain/entities/planet";
import {
  TPaginatedResourceResponse,
  TResourceName,
} from "@/domain/entities/resource";
import { IStarship } from "@/domain/entities/starships";

export interface IResourceRepository {
  getAll<T extends TResourceName>(
    resource: T,
    page: number,
    query?: string,
  ): Promise<TPaginatedResourceResponse>;
  getOne<T extends TResourceName>(
    resource: T,
    id: string,
  ): Promise<TResourceGetOneResult<T>>;
}

export type TResourceGetOneResult<T> = T extends "films"
  ? Promise<IFilm>
  : T extends "people"
    ? Promise<IPerson>
    : T extends "starships"
      ? Promise<IStarship>
      : T extends "planets"
        ? Promise<IPlanet>
        : never;
