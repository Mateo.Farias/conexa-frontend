export interface ISearchParams {
  page?: string;
  query?: string;
}
