import { Paginated } from "./paginated";

export type TResourceName = "films" | "planets" | "people" | "starships";

export interface Resource {
  id: string;
  name: string;
  created: string;
  edited: string;
}

export interface SavedResource extends Resource {
  type: TResourceName;
}

export type TPaginatedResourceResponse = Paginated<Resource>;
