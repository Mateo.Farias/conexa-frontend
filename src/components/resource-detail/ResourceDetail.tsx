import { TResourceName } from "@/domain/entities/resource";
import { TranslateResourceProperties } from "@/domain/interactors/TranslateResourceProperties";
import BaseContainer from "../containers/BaseContainer";
import Link from "next/link";
import { ResourceConfiguration } from "@/domain/drivers/configuration/ResourceConfiguration";

interface Props {
  resource: TResourceName;
  id: string;
}

export default async function ResourceDetail({ resource, id }: Props) {
  const getResourceDetail = new ResourceConfiguration().getOneResource();
  const resourceDetail = await getResourceDetail.get(resource, id);
  const translatedResource =
    new TranslateResourceProperties().translateResource(
      resource,
      resourceDetail,
    );

  return (
    <BaseContainer>
      <ul className="mx-auto flex flex-col text-justify">
        {translatedResource.map((attr, id) => {
          return (
            <li key={id}>
              <strong>{attr.key}</strong>: <small>{attr.value}</small>
            </li>
          );
        })}
      </ul>
    </BaseContainer>
  );
}
