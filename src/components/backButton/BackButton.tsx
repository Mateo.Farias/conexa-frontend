import Link from "next/link";
import { Button } from "../ui/button";

interface Props {
  path: string;
}

const BackButton: React.FC<Props> = ({ path }: Props) => {
  return (
    <Link href={path}>
      <Button>{"<"} Volver</Button>
    </Link>
  );
};

export default BackButton;
