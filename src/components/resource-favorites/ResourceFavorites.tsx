"use client";

import useFavorites from "@/hooks/useFavorites";
import Link from "next/link";

const ResourceFavorites: React.FC = () => {
  const { favorites } = useFavorites();
  return (
    <section
      className="mx-auto flex
    w-full flex-col justify-center gap-1 border-2 border-black bg-red-200
    bg-[radial-gradient(#cacbce_1px,transparent_1px)] p-2 text-center [background-size:16px_16px]"
    >
      <h1 className="text-2xl font-bold">Tus Favoritos</h1>
      {favorites.map((resource) => (
        <div key={`${resource.id} ${resource.type}`} className="bg-pink-100">
          <Link href={`/category/${resource.type}/${resource.id}`}>
            <span>{resource.name}</span>
          </Link>
        </div>
      ))}
    </section>
  );
};

export default ResourceFavorites;
