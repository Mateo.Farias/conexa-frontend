import { ReactNode } from "react";

interface Props {
  children?: ReactNode;
}

const BaseContainer: React.FC<Props> = ({ children }: Props) => {
  return (
    <section
      className=" bg-complementary mx-auto
      flex w-full flex-col justify-center gap-1 border-2 border-black
      bg-[radial-gradient(#cacbce_1px,transparent_1px)] p-2 text-center [background-size:16px_16px]"
    >
      {children}
    </section>
  );
};

export default BaseContainer;
