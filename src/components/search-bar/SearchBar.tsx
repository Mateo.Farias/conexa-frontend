"use client";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { Input } from "../ui/input";
import { useDebouncedCallback } from "use-debounce";

interface Props {}

const SearchBar: React.FC<Props> = ({}: Props) => {
  const searchParams = useSearchParams();
  const query = searchParams.get("query")?.toString();
  const pathname = usePathname();
  const { replace } = useRouter();

  const handleChange = useDebouncedCallback((query: string) => {
    const params = new URLSearchParams(searchParams);
    params.set("page", "1");
    if (query) {
      params.set("query", query);
    } else {
      params.delete("query");
    }
    replace(`${pathname}?${params.toString()}`);
  }, 300);

  return (
    <div className="relative">
      <Input
        placeholder="Buscar..."
        onChange={(e) => {
          handleChange(e.target.value);
        }}
        defaultValue={query}
      />
    </div>
  );
};

export default SearchBar;
