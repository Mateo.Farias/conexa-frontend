import { fireEvent, render, screen } from "@testing-library/react";
import ResourceCard from "./ResourceCard";
import { TResourceName } from "@/domain/entities/resource";

describe("ResourceCard Component Tests", () => {
  const currentDate = new Date();
  const resource: TResourceName = "people";
  const mockId = "mockId";
  it("Renders a ResourceCard with elements", () => {
    const { container } = render(
      <ResourceCard
        id={mockId}
        created={currentDate}
        edited={currentDate}
        resource={resource}
        title="mockTitle"
        isFavorite={false}
        onFavoriteClick={() => {}}
      />,
    );
    screen.debug();
    expect(screen.getByText("mockTitle")).toBeDefined();
    expect(
      screen.getByText("Creado: " + currentDate.toLocaleDateString()),
    ).toBeDefined();
    expect(
      screen.getByText("Editado: " + currentDate.toLocaleDateString()),
    ).toBeDefined();
    expect(screen.getByRole("link")).toHaveProperty(
      "href",
      `http://localhost/category/${resource}/${mockId}`,
    );
    const svgIcon = container.getElementsByTagName("svg").item(0)?.outerHTML;
    expect(svgIcon).toContain("white");
  });

  it("Renders a ResourceCard with favorite prop active", () => {
    const { container } = render(
      <ResourceCard
        id={mockId}
        created={currentDate}
        edited={currentDate}
        resource={resource}
        title="mockTitle"
        isFavorite={true}
        onFavoriteClick={() => {}}
      />,
    );
    const svgIcon = container.getElementsByTagName("svg").item(0)?.outerHTML;
    expect(svgIcon).not.toContain("white");
    expect(svgIcon).toContain("#fecaca");
  });

  it("Renders a ResourceCard with favorite prop active", () => {
    const mockFn = jest.fn();
    const { container } = render(
      <ResourceCard
        id={mockId}
        created={currentDate}
        edited={currentDate}
        resource={resource}
        title="mockTitle"
        isFavorite={true}
        onFavoriteClick={mockFn}
      />,
    );
    const favoriteIcon = container.querySelector("#favoriteIcon");
    expect(favoriteIcon).toBeDefined();
    fireEvent.click(favoriteIcon as Element);
    expect(mockFn).toHaveBeenCalled();
  });
});
