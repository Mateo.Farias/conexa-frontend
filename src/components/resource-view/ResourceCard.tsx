import { TResourceName } from "@/domain/entities/resource";
import { HeartIcon } from "lucide-react";
import Link from "next/link";
import colors from "tailwindcss/colors";

interface Props {
  resource: TResourceName;
  title: string;
  id: string;
  created: Date;
  edited: Date;
  isFavorite: boolean;
  onFavoriteClick: () => void;
}

const ResourceCard: React.FC<Props> = ({
  title,
  id,
  resource,
  created,
  edited,
  isFavorite,
  onFavoriteClick,
}: Props) => {
  return (
    <div
      className="
        flex flex-col border-black bg-white bg-[radial-gradient(#cacbce_1px,transparent_1px)]
        p-1 shadow-[4px_4px_0px_0px_rgba(0,0,0,1)] hover:translate-x-[3px] hover:translate-y-[3px]
        hover:shadow-none
      "
    >
      <div className="flex flex-row justify-between">
        <h1 className="mb-1 text-center">
          <Link href={`/category/${resource}/${id}`}>{title}</Link>
        </h1>
        <div
          id="favoriteIcon"
          className="cursor-pointer"
          onClick={onFavoriteClick}
        >
          <HeartIcon fill={isFavorite ? colors.red[200] : "white"} />
        </div>
      </div>
      <span className="text-xs">Creado: {created.toLocaleDateString()}</span>
      <span className="text-xs">Editado: {edited.toLocaleDateString()}</span>
    </div>
  );
};

export default ResourceCard;
