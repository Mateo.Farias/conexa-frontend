"use client";
import { Resource, TResourceName } from "@/domain/entities/resource";
import ResourceCard from "./ResourceCard";
import ResourceEmptyList from "./ResourceEmptyList";
import useFavorites from "@/hooks/useFavorites";

interface Props {
  resource: TResourceName;
  list: Resource[];
}

const Resources: React.FC<Props> = ({ list, resource }: Props) => {
  const { isResourceSavedInFavorites, handleFavoriteChange } = useFavorites();

  return (
    <section className="bg-complementary  bg-[radial-gradient(#cacbce_1px,transparent_1px)] [background-size:16px_16px]">
      {list.length ? (
        <div className="grid w-full grid-cols-2 gap-2 p-4 md:grid-cols-3">
          {list.map((item) => (
            <ResourceCard
              key={item.id}
              id={item.id}
              title={item.name}
              resource={resource}
              created={new Date(item.created)}
              edited={new Date(item.edited)}
              isFavorite={isResourceSavedInFavorites(item, resource)}
              onFavoriteClick={() => {
                handleFavoriteChange(item, resource);
              }}
            />
          ))}
        </div>
      ) : (
        <ResourceEmptyList />
      )}
    </section>
  );
};

export default Resources;
