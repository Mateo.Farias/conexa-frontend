import { TResourceName } from "@/domain/entities/resource";
import ResourcePagination from "../resource-pagination/ResourcePagination";
import SearchBar from "../search-bar/SearchBar";
import Resources from "./Resources";
import { ResourceConfiguration } from "@/domain/drivers/configuration/ResourceConfiguration";

interface Props {
  resource: TResourceName;
  currentPage: number;
  query?: string;
}

export default async function ResourceView({
  resource,
  currentPage,
  query,
}: Props) {
  const getResourceList = new ResourceConfiguration().getAllResource();
  const resourceList = await getResourceList.get(resource, currentPage, query);

  return (
    <div className="flex flex-col gap-4">
      <SearchBar />
      <Resources list={resourceList?.items ?? []} resource={resource} />
      {resourceList && <ResourcePagination totalPages={resourceList.pages} />}
    </div>
  );
}
