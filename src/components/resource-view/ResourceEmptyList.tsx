interface Props {}

const ResourceEmptyList: React.FC<Props> = ({}: Props) => {
  return (
    <div className="my-auto flex h-20 items-center justify-center">
      <h1 className="text-bold text-2xl">No se encontraron resultados</h1>
    </div>
  );
};

export default ResourceEmptyList;
