import NavLink from "./NavLink";

export interface NavbarItem {
  name: string;
  href: string;
}

interface Props {
  items: NavbarItem[];
}

const Navbar: React.FC<Props> = ({ items }: Props) => {
  return (
    <nav className="border-2 border-black bg-main shadow-[4px_4px_0px_0px_rgba(0,0,0,1)]">
      <ul className="text-xs font-bold md:text-lg">
        <div className="mx-auto flex flex-row items-center justify-between p-4 md:px-8">
          {items.map((item) => (
            <NavLink key={item.name} item={item} />
          ))}
        </div>
      </ul>
    </nav>
  );
};

export default Navbar;
