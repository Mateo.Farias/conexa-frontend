import Link from "next/link";
import { NavbarItem } from "./Navbar";

interface Props {
  item: NavbarItem;
}

const NavLink: React.FC<Props> = ({ item }: Props) => {
  return (
    <li className="underline-offset-4 hover:underline" key={item.name}>
      <Link href={item.href}>{item.name}</Link>
    </li>
  );
};

export default NavLink;
