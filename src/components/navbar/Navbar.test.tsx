import { render, screen } from "@testing-library/react";
import Navbar, { NavbarItem } from "./Navbar";

describe("Navbar Component Tests", () => {
  const item: NavbarItem = { href: "/", name: "mock1" };
  it("Renders a link correctly", () => {
    render(<Navbar items={[item]} />);
    const navLink = screen.getByText("mock1");
    expect(navLink).toBeDefined();
    expect(screen.getByRole("link", { name: "mock1" })).toHaveProperty(
      "href",
      "http://localhost/",
    );
  });

  it("Renders multiple nav links correctly", () => {
    render(<Navbar items={[item, { href: "/new", name: "mock2" }]} />);
    const navLink = screen.getByText("mock1");
    expect(navLink).toBeDefined();
    expect(screen.getByRole("link", { name: "mock1" })).toHaveProperty(
      "href",
      "http://localhost/",
    );
    const navLink2 = screen.getByText("mock2");
    expect(navLink2).toBeDefined();
    expect(screen.getByRole("link", { name: "mock2" })).toHaveProperty(
      "href",
      "http://localhost/new",
    );
  });
});
