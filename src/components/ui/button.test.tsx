import { fireEvent, render, screen } from "@testing-library/react";
import { Button } from "./button";

describe("Button tests", () => {
  it("Calls onClick function", () => {
    const mockFn = jest.fn();
    render(<Button onClick={mockFn}>mock</Button>);
    const button = screen.getByText("mock");
    expect(button).toBeDefined();
    fireEvent.click(button);
    expect(mockFn).toHaveBeenCalled();
  });
});
