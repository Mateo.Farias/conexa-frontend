import ResourceDetail from "@/components/resource-detail/ResourceDetail";

export default async function Page({ params }: { params: { id: string } }) {
  return <ResourceDetail id={params.id} resource="films" />;
}
