import ResourceView from "@/components/resource-view/ResourceView";
import { LoadingSpinner } from "@/components/ui/loading-spinner";
import { ISearchParams } from "@/domain/utils-types/PaginationSearchParams";
import { Suspense } from "react";

export default async function Page({
  searchParams,
}: {
  searchParams?: ISearchParams;
}) {
  const query = searchParams?.query ?? undefined;
  const currentPage = Number(searchParams?.page) || 1;

  return (
    <Suspense fallback={<LoadingSpinner />}>
      <ResourceView
        currentPage={currentPage}
        query={query}
        resource="planets"
      />
    </Suspense>
  );
}
