import BackButton from "@/components/backButton/BackButton";

export default function CategoryLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <>
      <BackButton path="/" />
      {children}
    </>
  );
}
