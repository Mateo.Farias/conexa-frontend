import Navbar, { NavbarItem } from "@/components/navbar/Navbar";
import type { Metadata } from "next";
import "./globals.css";

export const metadata: Metadata = {
  title: "Star Wars Enciclopedia",
  description: "All Star Wars knowledge in one place",
};

const navbarItems: NavbarItem[] = [
  { name: "Personajes", href: "/category/people" },
  { name: "Peliculas", href: "/category/films" },
  { name: "Naves", href: "/category/starships" },
  { name: "Planetas", href: "/category/planets" },
];

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body>
        <main
          className="flex h-full min-h-screen w-full flex-col gap-3
        bg-white bg-[radial-gradient(#cacbce_1px,transparent_1px)]
          px-2 py-2 [background-size:16px_16px] md:px-10 md:py-5
          "
        >
          <Navbar items={navbarItems} />
          {children}
        </main>
      </body>
    </html>
  );
}
