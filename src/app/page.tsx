import BaseContainer from "@/components/containers/BaseContainer";
import ResourceFavorites from "@/components/resource-favorites/ResourceFavorites";

export default function Home() {
  return (
    <>
      <BaseContainer>
        <h1 className="text-4xl font-bold">Star Wars Enciclopedia</h1>
        <p className="text-xl">
          Bienvenido a la enciclopedia, encontrá toda la información sobre tus
          personajes, películas, naves, etc. de tu saga favorita.
        </p>
        <p className="text-xl">
          Utiliza los links de arriba para ver el contenido de cada categoria.
        </p>
      </BaseContainer>
      <ResourceFavorites />
    </>
  );
}
