import { FavoriteResourceConfiguration } from "@/domain/drivers/configuration/FavoriteResourceConfiguration";
import {
  Resource,
  SavedResource,
  TResourceName,
} from "@/domain/entities/resource";
import { useEffect, useState } from "react";

const useFavorites = () => {
  const favoriteConfiguration = new FavoriteResourceConfiguration();
  const getAllFavorites = favoriteConfiguration.getAllFavorites();
  const saveFavorite = favoriteConfiguration.saveFavorite();
  const deleteFavorite = favoriteConfiguration.deleteFavorite();
  const [favorites, setFavorites] = useState<SavedResource[]>([]);

  useEffect(() => {
    const savedFavorites = getAllFavorites.get();
    setFavorites(savedFavorites);
  }, []);

  const handleFavoriteChange = (resource: Resource, type: TResourceName) => {
    if (isResourceSavedInFavorites(resource, type)) {
      const remainingFavorites = deleteFavorite.delete(resource.id, type);
      setFavorites([...remainingFavorites]);
    } else {
      const newFavorites = saveFavorite.save(resource, type);
      setFavorites([...newFavorites]);
    }
  };

  const isResourceSavedInFavorites = (
    resource: Resource,
    type: TResourceName,
  ): boolean => {
    return favorites.some(
      (favoriteResource) =>
        favoriteResource.id == resource.id && favoriteResource.type == type,
    );
  };

  return {
    favorites,
    handleFavoriteChange,
    isResourceSavedInFavorites,
  };
};

export default useFavorites;
