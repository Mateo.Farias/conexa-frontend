module.exports = {
  testPathIgnorePatterns: ["/.next/", "/node_modules/"],
  testEnvironment: "jsdom",
  transform: {
    "^.+\\.(js|jsx|ts|tsx)$": "<rootDir>/node_modules/babel-jest",
  },
};
