# Conexa Full-Stack Challenge - Frontend

NextJS project - web app that shows star wars data.

Uses Clean architecture structure, for maintainability and scalability.

- Tailwind for css styling
- BaseUI components from shadcn - [https://ui.shadcn.com/]
- Neobrutalism as styling guide

## Installation

** Requires node v16 or higher **

Fill .env template with backend host

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run dev

# production mode
$ npm run prod
```
